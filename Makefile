CPPFLAGS+=-Wall -Wextra -Wpedantic -Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline

CFLAGS+=-std=c11
LDLIBS+=-lm

BIN=risk
OBJS=risk.o

.PHONY: clean debug

$(BIN): $(OBJS)

debug: CFLAGS+=-g
debug: $(BIN)

clean:
	$(RM) $(OBJS) $(BIN)
