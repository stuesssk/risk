#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <time.h>


struct territory {
	int player;
    int troops;
    int id;
};

struct continent {
    struct territory *territories;
    int numteritories;
};

int continentCount = 6;
int territoryCount = 30;

int
displayMenu();

struct
continent **newgame(struct continent **continents);

int
combat(int *offenseNum, int *defenseNum);

int
sort(const void *a, const void *b);

void
printDice(int a);

void
playgame(struct continent **continents);

//void
//drawmap(struct continent *one);



int main(void)
{
    int choice = displayMenu();

    // seeding RNGesus
    srand((unsigned)time(NULL));


    // Mallocing the appropriate memory size, finally figured out 
    // the appropraite order of pointers and structs,.abcds*
    struct continent **continents;
    continents = calloc(continentCount,sizeof(struct continent*));
    for (int i = 0; i < continentCount; ++i)
    {
        continents[i] = calloc(continentCount,sizeof(struct continent));
        continents[i]->territories = calloc(continentCount * territoryCount,
                                            sizeof(continents[i]->territories));
    }

    switch (choice)
    {
    case 1:
        //New game
        continents = newgame(continents);
        break;
    case 2:
        // Load game
        return(0);
        break;
    case 3:
        // Exit Game
        return(0);
    }
    playgame(continents);
}

int
displayMenu()
{

    //Idea for displaying menu obtained from alex dow
    FILE *menu = fopen("menu.txt", "r");

    if (!menu)
    {
        return EX_NOINPUT;
    }
    char *line = calloc(512,sizeof(char));
    while (fgets(line,500,menu))
    {
        printf("%s",line);
    }

    free(line);
    fclose(menu);
    printf("Enter an Option:");
    while (true)
    {
        int choice = -1;
        char tmp; 
        while ((tmp = fgetc(stdin)) != EOF && tmp != '\n')
        {
            if (choice == -1)
            {
                choice = strtol(&tmp,NULL,10);
            }
        }
        if (choice > 0 && choice <= 3)
        {
            return choice;
        }
        else
        {
            printf("Please Enter A Valid Choice.");
        }
    }
 
}

struct 
continent ** newgame(struct continent **continents)
{
    territoryCount = 0;
    for (int i = 0; i < continentCount ; ++i)
    {
        for (int j = 0; j < 6 ; ++j)
        {
            continents[i]->territories[j].id = (6 * i) + j;
            printf("ID[%d][%d] %d\n",i,j,continents[i]->territories[j].id);
            continents[i]->territories[j].troops = 10;
            printf("troops[%d][%d] %d\n",i,j,continents[i]->territories[j].troops);
            continents[i]->territories[j].player = (j + (6 * i)) % 2;
            printf("player[%d][%d] %d\n",i,j,continents[i]->territories[j].player);
        }
    }
    //drawmap(continents);
    return continents;
}

int
combat(int *offenseNum, int *defenseNum)
{

    int offenseDice[3] = {0};
    int defenseDice[2] = {0};
    while (*offenseNum > 0 && *defenseNum > 0)
    {
        printf("Off %d\n",*offenseNum);
        printf("Def %d\n",*defenseNum);

        for (int i = 0; i < 3; ++i)
        {
            offenseDice[i] = rand() % 6 + 1;
        }
        switch (*offenseNum)
        {
        case 2:
            offenseDice[1] = 0;
            offenseDice[2] = 0;
            break;
        case 3:
            offenseDice[2] = 0;
            break;
        default:
            break;
        }
        qsort(offenseDice, 3, sizeof(*offenseDice), sort);


        for (int i = 0; i < 2; ++i)
        {
            defenseDice[i] = rand() % 6 + 1;
        }

        if (*defenseNum == 1)
        {
            defenseDice[1] = 0;
        }
        qsort(defenseDice, 2, sizeof(*defenseDice), sort);
        

        if (offenseDice[0] > defenseDice[0])
        {
            *defenseNum -= 1;
            
        }
        else
        {
            *offenseNum -= 1;
        }
        printf("Offense ");
        printDice(offenseDice[0]);
        printf("Defense ");
        printDice(defenseDice[0]);


        if (offenseDice[1] == 0 || defenseDice[1] == 0)
        {
            continue;

            
        }
        else if (offenseDice[1] > defenseDice[1])
        {
            *defenseNum -= 1;
            printf("Offense ");
            printDice(offenseDice[1]);
            printf("Defense ");
            printDice(defenseDice[1]);
        }
        else
        {
            *offenseNum -= 1;
            printf("Offense ");
            printDice(offenseDice[1]);
            printf("Defense ");
            printDice(defenseDice[1]);
        }

        printf("Off %d\n",*offenseNum);
        printf("Def %d\n",*defenseNum);

        // Give offense chance to chicken out of the battle
        if (*offenseNum > 1)
        {
            printf("Attacker still wish to continue(y/n)");
            char choice;
            while(true){
                while(true)
                {
                    char buff[8]; // Allow them to misskey a few characters
                    fgets(buff,sizeof(buff),stdin);
                    // I'll be nice if they fat finger they can have a 
                    // redo on their guess
                    if (strlen(buff) == 2){
                        choice = buff[0];
                        choice = tolower(choice);
                        break;
                    }
                    printf("Entered more than one Character, enter again:");
                }

                if (choice == 'n')
                {
                    printf("Off %d\n",*offenseNum);
                    printf("Def %d\n",*defenseNum);
                    return choice;
                }
                else if (choice == 'y')
                {
                    break;
                }
                else
                {
                    printf("Please Enter A Valid Choice.");
                }
            }
        }


    }
    printf("Off %d\n",*offenseNum);
    printf("Def %d\n",*defenseNum);
    return 1;
}

int
sort(const void *a, const void *b)
{
    const int *ia = (const int *)a;
    const int *ib = (const int *)b;
    return *ib - *ia;
}

void
printDice(int a)
{
    switch (a)
    {
    case 0:
        break;
    case 1:
        printf("⚀\n");
        break;
    case 2:
        printf("⚁\n");
        break;
    case 3:
        printf("⚂\n");
        break;
    case 4:
        printf("⚃\n");
        break;
    case 5:
        printf("⚄\n");
        break;
    case 6:
        printf("⚅\n");
        break;
    default:
        printf("ERROR\n");
    }
}

void
playgame(struct continent **continents)
{
    int offenseNum;
    int defenseNum;
    int offenseTerritory;
    int defenseTerritory;

play:
    printf("Attacking Territory? ");
    while (true)
    {
        while (true)
        {
            char buff[8]; // Allow them to misskey a few characters
            fgets(buff,sizeof(buff),stdin);
            // I'll be nice if they fat finger they can have a 
            // redo on their guess
            if (strlen(buff) <= 3)
            {
                offenseTerritory = buff[0];
                offenseTerritory -= 48;
                break;
            }
            printf("Entered more than one Character, enter again:");
        }
        if (offenseTerritory >= 0 || offenseTerritory <= 4)
        {
             printf("Off %d\n",offenseTerritory);
             break;
        }
        else
        {
           printf("Please Enter A Valid Choice.");
        }
    }

    printf("Defending Territory? ");
    while (true)
    {
        while (true)
        {
            char buff[8]; // Allow them to misskey a few characters
            fgets(buff,sizeof(buff),stdin);
            // I'll be nice if they fat finger they can have a 
            // redo on their guess
            if (strlen(buff) == 2)
            {
                defenseTerritory = buff[0];
                defenseTerritory -= 48;
                break;
            }
            printf("Entered more than one Character, enter again:");
        }
        if (defenseTerritory >= 0 || defenseTerritory <= 4)
        {
             printf("Def %d\n",defenseTerritory);
             break;
        }
        else
        {
           printf("Please Enter A Valid Choice.");
        }
    }

    offenseNum = continents[1]->territories[offenseTerritory].troops - 1;
    defenseNum = continents[1]->territories[defenseTerritory].troops; 

    int attackTroops = offenseNum;
    printf("Attcking with how many troops(Enter for all):");
    char *troops = calloc(32, sizeof(*troops));

    while (true)
    {
        fgets(troops, 31, stdin);
        if (*troops == '\n')
        {
            printf("Attacking with all the troops.\n");
            break;
        }

        attackTroops = strtol(troops, NULL, 10);
        printf("troop count = %d\n", attackTroops);

        if (attackTroops > 1 && attackTroops <= offenseNum)
        {
            printf("%d\n",attackTroops);
            offenseNum = attackTroops;
            break;
        }
        else
        {
            printf("Please Enter A Valid Choice.");
        }
    }
    combat(&offenseNum, &defenseNum);
    
    printf("New offense:%d\n",offenseNum);
    continents[1]->territories[offenseTerritory].troops -= (attackTroops - offenseNum);
    printf("#: %d\n",continents[1]->territories[offenseTerritory].troops);
    continents[1]->territories[defenseTerritory].troops = defenseNum;
    if (offenseNum > 0 && defenseNum == 0)
    {
        continents[1]->territories[defenseTerritory].troops = (offenseNum);
        continents[1]->territories[defenseTerritory].player = 
                              continents[1]->territories[offenseTerritory].player;
    }
        

    int players[2] = {0};
    for (int i = 0; i < 4; ++i)
    {
        switch (continents[1]->territories[i].player)
        {
        case 0:
            players[0] += 1;
            break;
        case 1:
            players[1] += 1;
            break;
        case 2:
            players[2] += 1;
            break;
        case 3:
            players[3] += 1;
            break;
        default:
            printf("Crashing, unexpected player number!?");
            exit(1);
        }
        if (players[i] == territoryCount)
        {
            printf("Player %d Wins",i);
            exit(0);
        }
    }
    goto play;
    
}

/*void
drawmap(struct continent **one)
{

}*/
